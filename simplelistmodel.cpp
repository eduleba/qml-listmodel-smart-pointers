#include "simplelistmodel.h"

#include <QMetaObject>
#include <QMetaProperty>

#include <iostream>

SimpleListModel::SimpleListModel(QObject* parent)
    : QAbstractListModel(parent)
{

}

void SimpleListModel::Add(std::shared_ptr<QObject> item)
{
    if (roles_map_.empty()){
      PrepareRolesMap(item);
    }

    beginInsertRows(QModelIndex(), data_.size(), data_.size());
    data_.append(item);
    endInsertRows();
}

void SimpleListModel::Clean()
{
    beginRemoveRows(QModelIndex(), 0, data_.size());
    data_.clear();
    endRemoveRows();
}

std::shared_ptr<QObject> SimpleListModel::Get(int index)
{
    return data_[index];
}

std::shared_ptr<QObject> SimpleListModel::GetByProperty(QString poperty, QVariant value)
{
    for(auto & item : data_){
        if(item->property(poperty.toStdString().c_str()) == value){
            return item;
        }
    }
    return nullptr;
}

int SimpleListModel::rowCount(const QModelIndex &p) const
{
    Q_UNUSED(p)
    return data_.size();
}

QVariant SimpleListModel::data(const QModelIndex &index, int role) const
{
    return data_[index.row()]->property(roles_map_[role]);
}

QHash<int, QByteArray> SimpleListModel::roleNames() const
{
    return roles_map_;
}

void SimpleListModel::PrepareRolesMap(std::shared_ptr<QObject> item)
{
    // This uses QObject properties as roles in model
    int role = Qt::UserRole + 1;
    const QMetaObject* metaObj = item.get()->metaObject();
    for(int i = metaObj->propertyOffset(); i < metaObj->propertyCount(); ++i){
      roles_map_[role] = metaObj->property(i).name();
      role++;
    }
}
