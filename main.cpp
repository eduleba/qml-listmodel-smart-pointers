#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <simplelistmodel.h>
#include <user.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    std::shared_ptr<SimpleListModel> model = std::make_shared<SimpleListModel>();

    engine.rootContext()->setContextProperty(
                     "UsersListModel",
                     model.get());

    std::shared_ptr<User> u0 = std::make_shared<User>();
    u0->SetID(0);
    u0->SetUserName("User 0");

    std::shared_ptr<User> u1 = std::make_shared<User>();
    u1->SetID(1);
    u1->SetUserName("User 1");

    std::shared_ptr<User> u2 = std::make_shared<User>();
    u2->SetID(2);
    u2->SetUserName("User 2");

    model->Add(u0);
    model->Add(u1);
    model->Add(u2);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
