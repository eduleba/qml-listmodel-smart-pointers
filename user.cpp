#include "user.h"

User::User(QObject *parent) : QObject(parent)
{

}

QString User::GetUserName() const
{
    return m_user_name;
}

int User::GetID() const
{
    return m_id;
}

void User::SetUserName(QString user_name)
{
    if (m_user_name == user_name)
        return;

    m_user_name = user_name;
    emit signalUserNameChanged(m_user_name);
}

void User::SetID(int id)
{
    if (m_id == id)
        return;

    m_id = id;
    emit signalIDChanged(m_id);
}
