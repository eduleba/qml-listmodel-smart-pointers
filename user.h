#ifndef USER_H
#define USER_H

#include <QObject>

class User : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString user_name READ GetUserName WRITE SetUserName NOTIFY signalUserNameChanged)
    Q_PROPERTY(int id READ GetID WRITE SetID NOTIFY signalIDChanged)


public:
    explicit User(QObject *parent = nullptr);

    QString GetUserName() const;

    int GetID() const;

signals:
    void signalUserNameChanged(QString user_name);
    void signalIDChanged(int id);

public slots:
    void SetUserName(QString user_name);
    void SetID(int id);

private:
    QString m_user_name;
    int m_id;
};

#endif // USER_H
