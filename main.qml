import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    ListView
    {
        anchors.fill: parent
        model: UsersListModel
        delegate: Item {
            height: 40
            Text {
                id: content
                anchors.fill: parent
                text: "ID: " + model.id + "\tName: " + model.user_name
            }
        }
    }
}
